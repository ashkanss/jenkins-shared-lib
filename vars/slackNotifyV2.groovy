def call(Map params) {
    def blocks = [];
    def status_text = ''
    if(params.title) {
        switch(params.status) { 
            case "success":
                status_text = ':ballot_box_with_check: Success'
                break; 
            case "failure":
                status_text = 'Failed :x:' 
                break;
            case "start":
                status_text = 'Running :arrow_forward:' 
                break;
            default:
                status_text = ''
                break; 
        } 
        blocks.add(
            [
                "type": "header",
                "text": 
                [
                    "type": "plain_text",
                    "text": " ${params.title} | ${status_text}",
                    "emoji": true
                ]
            ]
        )
    }
    def elements = [];
    if(params.props.size() > 0) {
        def details = ""
        for(prop in params.props) { 
            details += prop + "\n"
        }
        blocks.add(
            [
                "type": "context",
                "elements": [
                    [
                        "type": "mrkdwn",
                        "text": "${details}"
                    ]
                ]
            ]
        )
    }
    elements = [];
    if(params.buttons.size() > 0) {
        for(button in params.buttons) { 
            elements.add(
                [
                    "type": "button",
                    "text": [
                        "type": "plain_text",
                        "text": "${button.text}"
                    ],
                    "url": "${button.url}"
                ]
            )
        }
        blocks.add(
            [
                "type": "actions",
                "elements": elements
            ]
        )
    }
    blocks.add(
        [
            "type": "divider"
        ]
    )
    slackSend(blocks: blocks, botUser: true, channel: params.channel)
}