def call(Map params) {
    ftpFileName = "${params.ftpFileName}-${params.majorVersion}.${params.minorVersion}.${params.buildNumber}.zip"
    withCredentials([usernamePassword(credentialsId: 'ftp-jenkins-user', usernameVariable: 'FTP_CRED_USR', passwordVariable: 'FTP_CRED_PSW')]) {
        winRMClient (
            credentialsId: 'asharifi-winrm-cred', 
            hostName: params.hostname,
            winRMOperations: [
                invokeCommand("""
                    function createDir(${'$'}pathName) {
                        If(!(test-path ${'$'}pathName))
                        {
                              New-Item -ItemType Directory -Force -Path ${'$'}pathName
                        }
                    }
                    # Creates workspace dir if not exists
                    createDir ${params.workspace}
                    
                    # Clears workspace
                    Remove-Item ${params.workspace}\\* -Recurse -Force
                    
                    #Go to workspace
                    Set-Location ${params.workspace};
                    
                    # Download bin from FTP
                    wget ftp://${FTP_CRED_USR}:${FTP_CRED_PSW}@ftp.bmsalpha.com/${params.ftpPath}/${params.ftpFileName}.zip -OutFile publish.zip;
                    
                    # Unzip the files downloaded
                    Add-Type -assembly "system.io.compression.filesystem";
                    [io.compression.zipfile]::ExtractToDirectory("${params.workspace}\\publish.zip", "${params.workspace}\\publish")
                    
                    # Creates backup dir if not exists
                    createDir ${params.backup}
                    
                    # Create a Backup
                    #${'$'}date = Get-Date -Format "MM-dd-yyyy-HH.mm.ss";
                    #[io.compression.zipfile]::CreateFromDirectory("${params.appPath}", "${params.backup}\\${'$'}date.zip")
                    
                    # Stop Site and app pool
                    C:\\Windows\\System32\\inetsrv\\appcmd stop apppool /apppool.name:"${params.projectName}"
                    C:\\Windows\\System32\\inetsrv\\appcmd stop site /site.name:"${params.projectName}"
                    
                    # Update the App with the new code
                    Copy-Item -Force -Recurse ${params.workspace}\\publish\\* ${params.appPath}
                    
                    # Start Site and app pool
                    C:\\Windows\\System32\\inetsrv\\appcmd start site /site.name:"${params.projectName}"
                    C:\\Windows\\System32\\inetsrv\\appcmd start apppool /apppool.name:"${params.projectName}"
                """)
            ]
        )
    }
}