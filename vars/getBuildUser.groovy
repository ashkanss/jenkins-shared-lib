def call() {
    wrap([$class: 'BuildUser']) {
        return "${env.BUILD_USER} / ${env.BUILD_USER_ID}"
    }
}